package com.ctz.tracker;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.models.consumer.PNPublishResult;
import com.pubnub.api.models.consumer.PNStatus;

public class Main {

	Connection con_avls = null;
	private final Object LOCK = true;
	public static String dataip, port, companyname, builddate, versionno,
			driver, url, username, password;
	public static int maxConnections, initialConnections;
	public static boolean e_dispatch = false;
	static Runtime javaRuntime;
	static PubNub pubNub;

	public static void main(String[] args) throws IOException {

		// TODO code application logic here

		try {
			// Pubnub pubnub = new
			// Pubnub("pub-c-a64317a5-a592-4a93-86a5-470684e385f0","sub-c-be626cbc-18f1-11e6-8b91-02ee2ddab7fe");

			PNConfiguration pnConfiguration = new PNConfiguration();
			pnConfiguration
					.setSubscribeKey("sub-c-be626cbc-18f1-11e6-8b91-02ee2ddab7fe");
			pnConfiguration
					.setPublishKey("pub-c-a64317a5-a592-4a93-86a5-470684e385f0");

			pubNub = new PubNub(pnConfiguration);
			javaRuntime = Runtime.getRuntime();
			javaRuntime.maxMemory();
			java.io.File currentDir = new java.io.File("");

			File file = new File(currentDir.getAbsolutePath() + "/Config.xml");
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(file);
			doc.getDocumentElement().normalize();

			NodeList nodeLst = doc.getElementsByTagName("SETTING");

			for (int s = 0; s < nodeLst.getLength(); s++) {
				Node fstNode = nodeLst.item(s);

				if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
					Element Elmnts = (Element) fstNode;

					// DATAIP
					NodeList DataIPNmElmntLst = Elmnts
							.getElementsByTagName("DATA_IP");
					Element DataIPElmnt = (Element) DataIPNmElmntLst.item(0);
					NodeList DataIPNm = DataIPElmnt.getChildNodes();
					dataip = ((Node) DataIPNm.item(0)).getNodeValue();

					// DATAPORT
					NodeList DataPORTNmElmntLst = Elmnts
							.getElementsByTagName("DATA_PORT");
					Element DataPORTNmElmnt = (Element) DataPORTNmElmntLst
							.item(0);
					NodeList DataPORTNm = DataPORTNmElmnt.getChildNodes();
					port = ((Node) DataPORTNm.item(0)).getNodeValue();

					// COMPANY NAME

					NodeList CNameNmElmntLst = Elmnts
							.getElementsByTagName("COMPANY_NAME");
					Element CNameNmElmnt = (Element) CNameNmElmntLst.item(0);
					NodeList CNameNm = CNameNmElmnt.getChildNodes();
					companyname = ((Node) CNameNm.item(0)).getNodeValue();

					// BUILD DATE
					NodeList BuildNmElmntLst = Elmnts
							.getElementsByTagName("BUILD_DATE");
					Element BuildNmElmnt = (Element) BuildNmElmntLst.item(0);
					NodeList BuildNm = BuildNmElmnt.getChildNodes();
					builddate = ((Node) BuildNm.item(0)).getNodeValue();

					// VERSIONNO
					NodeList VersionNmElmntLst = Elmnts
							.getElementsByTagName("VERSIONNO");
					Element VersionNmElmnt = (Element) VersionNmElmntLst
							.item(0);
					NodeList VersionNm = VersionNmElmnt.getChildNodes();
					versionno = ((Node) VersionNm.item(0)).getNodeValue();

					// Driver Key
					NodeList DriverNmElmntLst = Elmnts
							.getElementsByTagName("DRIVER");
					Element DriverElmnt = (Element) DriverNmElmntLst.item(0);
					NodeList DriverNm = DriverElmnt.getChildNodes();
					driver = ((Node) DriverNm.item(0)).getNodeValue();

					// URL
					NodeList URLNmElmntLst = Elmnts.getElementsByTagName("URL");
					Element URLNmElmnt = (Element) URLNmElmntLst.item(0);
					NodeList URLNm = URLNmElmnt.getChildNodes();
					url = "" + ((Node) URLNm.item(0)).getNodeValue();

					// IP
					NodeList IPNmElmntLst = Elmnts
							.getElementsByTagName("DB_IP");
					Element IPNmElmnt = (Element) IPNmElmntLst.item(0);
					NodeList IPNm = IPNmElmnt.getChildNodes();
					// url = url + ((Node) IPNm.item(0)).getNodeValue() + "/";
					url = url + ((Node) IPNm.item(0)).getNodeValue() + ";";

					// DB Name
					NodeList DBNmElmntLst = Elmnts
							.getElementsByTagName("DB_NAME");
					Element DBNmElmnt = (Element) DBNmElmntLst.item(0);
					NodeList DBNm = DBNmElmnt.getChildNodes();
					// url = url + ((Node) DBNm.item(0)).getNodeValue();//
					// "?user=";
					url = url + "databaseName="
							+ ((Node) DBNm.item(0)).getNodeValue() + ";user=";
					// USER ID
					NodeList UserNmElmntLst = Elmnts
							.getElementsByTagName("DB_USER");
					Element UserNmElmnt = (Element) UserNmElmntLst.item(0);
					NodeList UserNm = UserNmElmnt.getChildNodes();
					url = url + ((Node) UserNm.item(0)).getNodeValue()
							+ ";password=";
					username = ((Node) UserNm.item(0)).getNodeValue();

					// PASSWORD
					NodeList PwdNmElmntLst = Elmnts
							.getElementsByTagName("DB_PWD");
					Element PwdNmElmnt = (Element) PwdNmElmntLst.item(0);
					NodeList PwdNm = PwdNmElmnt.getChildNodes();
					url = url + ((Node) PwdNm.item(0)).getNodeValue() + ";";
					password = ((Node) PwdNm.item(0)).getNodeValue();

					// MIN CONNECTIONS
					NodeList MINNmElmntLst = Elmnts
							.getElementsByTagName("MIN_CONNECTION");
					Element MINNmElmnt = (Element) MINNmElmntLst.item(0);
					NodeList MINNm = MINNmElmnt.getChildNodes();
					initialConnections = Integer
							.parseInt(((Node) MINNm.item(0)).getNodeValue());

					// MAXCONNECTIONS
					NodeList MAXNmElmntLst = Elmnts
							.getElementsByTagName("MAX_CONNECTION");
					Element MAXNmElmnt = (Element) MAXNmElmntLst.item(0);
					NodeList MAXNm = MAXNmElmnt.getChildNodes();
					maxConnections = Integer.parseInt(((Node) MAXNm.item(0))
							.getNodeValue());

					// EnableDispatch
					NodeList DisNmElmntLst = Elmnts
							.getElementsByTagName("ENABLE_DISPATCH");
					Element DisNmElmnt = (Element) DisNmElmntLst.item(0);
					NodeList DisNm = DisNmElmnt.getChildNodes();
					e_dispatch = Boolean.parseBoolean(((Node) DisNm.item(0))
							.getNodeValue());

					String[] sport = port.split(",");
					ExecutorService executor = Executors
							.newFixedThreadPool(sport.length + 6);

				}
			}

		} catch (Exception e) {
			// LogMessage.errorfilecreation("Error inside the InsertResponseMessage:- "
			// gc();
			// + e.getMessage());
		}

		new Main().startServer();


//		boolean restart;
//		do {
//			restart = new Main().startServer();
//		} while (restart);
	}

	public void startServer() {
		final ExecutorService clientProcessingPool = Executors
				.newFixedThreadPool(10);

		Runnable serverTask = new Runnable() {
			public void run() {
				try {
					ServerSocket serverSocket = new ServerSocket(1339);
					System.out.println("Waiting for clients to connect...");
					while (true) {
						Socket clientSocket = serverSocket.accept();
						clientProcessingPool
								.submit(new ClientTask(clientSocket));
					}
				} catch (IOException e) {
					LogMessage
							.errorfilecreation("Unable to process client request"
									+ e.getMessage());

				}
			}
		};
		Thread serverThread = new Thread(serverTask);
		serverThread.start();
//		return true;
	}

	private class ClientTask implements Runnable {
		private final Socket clientSocket;

		private ClientTask(Socket clientSocket) {
			this.clientSocket = clientSocket;
		}

		public void run() {
			// System.out.println("Got a client !");
			try {
				int red = -1;
				byte[] buffer = new byte[5 * 1024]; // a read buffer of 5KiB
				byte[] redData;
				// String clientData = new String();
				String clientData = null;
				while ((red = clientSocket.getInputStream().read(buffer)) > -1) {
					redData = new byte[red];
					System.arraycopy(buffer, 0, redData, 0, red);
					clientData = new String(redData, "UTF-8");

					if (clientData.contains("^TMPER")) {

//						 System.out.println("String Received " + clientData);
						String[] st = clientData.split("^TMPER");
						for (String dpackt : st) {
							if (!dpackt.equals("")) {
								String datapackt = "^TMPER" + dpackt;
								if (datapackt.contains("^TMPER")) {
									try {
										TMDataPacket(datapackt);
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								} else {

								}
							}
						}

					} else if (clientData.contains("^TMALT")) {

						String[] st = clientData.split("^TMALT");
						for (String dpackt : st) {
							if (!dpackt.equals("")) {
								String datapackt = "^TMALT" + dpackt;
								if (datapackt.contains("^TMALT")) {
//									TMAlertPacket(datapackt);
								} else {

								}
							}
						}

					}

				}

				clientSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		private void TMAlertPacket(String datapackt) throws IOException {
			String unitNo = "";
			double lait = 0;
			double logit = 0;
			String utcTime = "";
			String utcDate = "";
			String heading = "";
			String speed = "";

			String alerttype = "";
			String[] str = datapackt.split(",");
			int len = str.length;
			String dt = "";
			try {
				// ^TMALT|354868055694931|1|6|1|0.00000|0.00000|0|0|0.0|0|#

				unitNo = str[1];
				lait = Double.parseDouble(str[5].trim());
				logit = Double.parseDouble(str[6].trim());
				utcTime = str[7];
				utcDate = str[8];
				speed = str[9];
				heading = str[10];
				alerttype = str[3];

				if (!utcDate.equals("") || utcDate.equals("0")) {
					utcDate = "20" + utcDate.substring(4, 6) + "-"
							+ utcDate.substring(2, 4) + "-"
							+ utcDate.substring(0, 2);
				}
				if (!utcTime.equals("") || utcTime.equals("0")) {
					utcTime = utcTime.substring(0, 2) + ":"
							+ utcTime.substring(2, 4) + ":"
							+ utcTime.substring(4, 6);
					dt = utcDate + " " + utcTime;
				}
				// System.out.println(dt);
				DateFormat utcFormat = new SimpleDateFormat(
						"dd-MM-yyyy HH:mm:ss");
				utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
				Date date;
				if (utcTime.equals("") || utcTime.equals(" 00:00:00")
						|| utcTime.equals("0")) {
					date = new Date();
				} else {
					date = utcFormat.parse(dt);
				}
				DateFormat pstFormat = new SimpleDateFormat(
						"dd-MM-yyyy HH:mm:ss");
				pstFormat.setTimeZone(TimeZone.getTimeZone("GMT+530"));
				String utcgmt = "";

				// InsertTMAPacket(unitNo, lait, logit, dt, speed, heading,
				// Integer.parseInt(alerttype));

			} catch (Exception e) {
				LogMessage
						.errorfilecreation("Error inside the InsertTMAPacket:- "
								+ e.getMessage());
			}

		}

		private void InsertTMAPacket(String unitNo, double lait, double logit,
				String dt, String speed, String heading, int alerttype)
				throws IOException {
			// TODO Auto-generated method stub

			try {
				// System.out.println("InsertTMAPacket");
				con_avls = JDBC.getConnection();

				CallableStatement insertStatement = con_avls
						.prepareCall("{call ListenerTrackMateAlert('" + unitNo
								+ "'," + "" + lait + "," + "" + logit + ","
								+ "'" + dt + "','" + speed + "'," + "'"
								+ heading + "'," + "" + alerttype + ") }");

				synchronized (LOCK) {
					// System.out.println(insertStatement);
					insertStatement.execute();

				}

			} catch (Exception se) {
				LogMessage
						.errorfilecreation("Error inside the InsertTMAPacket:- "
								+ se.getMessage());
			} finally {
				JDBC.closeConnection();
			}

		}

		public void InsertTMPacket(String unitNo, int pktSeq, double lait,
				double logit, Date dt, String speed, String heading,
				int ignition, int dinput1, int dinput2, String analog,
				String gOdometer, String pOdometer, int mainSupply, int gValid,
				int pStatus) throws IOException, SQLException {
			try {

				con_avls = JDBC.getConnection();

				CallableStatement insertStatement = con_avls
						.prepareCall("{call ListenerTrackMate('" + unitNo
								+ "'," + "" + pktSeq + "," + "" + lait + ","
								+ "" + logit + "," + "'" + dt + "','" + speed
								+ "'," + "'" + heading + "'," + "" + ignition
								+ "," + "" + dinput1 + "," + "" + dinput2 + ","
								+ "'" + analog + "'," + "'" + gOdometer + "',"
								+ "'" + pOdometer + "'," + "" + mainSupply
								+ "," + "" + gValid + "," + "" + pStatus
								+ ") }");
				// System.out.println("insertStatement" + insertStatement);

				synchronized (LOCK) {
					// System.out.println(insertStatement);
					insertStatement.execute();

				}

			} catch (Exception se) {
				LogMessage
						.errorfilecreation("Error inside the InsertTMPacket:- "
								+ se.getMessage());
			} finally {
				JDBC.closeConnection();
			}
		}

		public void TMDataPacket(String datapackt) throws Exception {

			// System.out.println("datapackt" + datapackt);
			String unitNo = "";
			double lait = 0;
			double logit = 0;
			String utcTime = "";
			String utcDate = "";
			String heading = "";
			String speed = "";
			int ignition = 0;
			String dinput1 = "";
			String dinput2 = "";
			String analog = "";
			String gOdometer = "";
			String pOdometer = "";
			String mainSupply = "";
			int gValid =0 ;
			int pStatus =0;

			String dt = "";
			int pktSeq = 0;
			String[] str = datapackt.split("\\|");
			int len = str.length;
			Date date = null;
			try {
				// ^TMPER|354868055694931|234|12.99829|77.54013|130035|240315|0.0|341.18|1|0|0|0.030|3.7|11.8|0.0|0.0|1|1|0|
				// ^TMPER|868050905399|1313|12.99838|77.54011|164344|070115|0.4||276.64|1|0|0|0.030|4.0|13.0|157.1|3.6|1|1|0|#
//			    	^TMPER|353218074676959|400|12.96903|77.75254|133905|080916|0.0|46.63|0|0|0|0.045|3.4|11.8|24.6|0.0|1|1|0|#

				// at : 07-01-2015 22:10:22
				unitNo = str[1];
				lait = Double.parseDouble(str[3].trim());
				logit = Double.parseDouble(str[4].trim());
				utcTime = str[5];
				utcDate = str[6];
				speed = str[7];
				heading = str[8];
				ignition = Integer.parseInt(str[9]);
				dinput1 = str[10];
				dinput2 = str[11];
				analog = str[12];
				gOdometer = str[15];
				pOdometer = str[16];
				mainSupply = str[17];
				gValid = Integer.parseInt(str[18]);
				pStatus = Integer.parseInt(str[19]);
				pktSeq = Integer.parseInt(str[2].trim());

				if (!utcDate.equals("")) {
					utcDate = "20" + utcDate.substring(4, 6) + "-"
							+ utcDate.substring(2, 4) + "-"
							+ utcDate.substring(0, 2);
				}
				if (!utcTime.equals("")) {
					utcTime = utcTime.substring(0, 2) + ":"
							+ utcTime.substring(2, 4) + ":"
							+ utcTime.substring(4, 6);
					dt = utcDate + " " + utcTime;
				}
				// System.out.println(dt);
				/*
				 * DateFormat utcFormat = new SimpleDateFormat(
				 * "dd-MM-yyyy HH:mm:ss");
				 */

				DateFormat utcFormat = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");

				if (utcTime.equals("") || utcTime.equals(" 00:00:00")) {
					date = new Date();
				} else {
					date = utcFormat.parse(dt);
				}


				// Commented to test device without connecting to vehicle
//				if (ignition == 1) {
//					InsertTMPacket(unitNo, pktSeq, lait, logit, date, speed,
//							heading, ignition, Integer.parseInt(dinput1),
//							Integer.parseInt(dinput2), analog, gOdometer,
//							pOdometer, Integer.parseInt(mainSupply),
//							Integer.parseInt(gValid), Integer.parseInt(pStatus));
//				}


					/*InsertTMPacket(unitNo, pktSeq, lait, logit, date, speed,
							heading, ignition, Integer.parseInt(dinput1),
							Integer.parseInt(dinput2), analog, gOdometer,
							pOdometer, Integer.parseInt(mainSupply),
							Integer.parseInt(gValid), Integer.parseInt(pStatus));*/
//				}

			} catch (Exception e) {
				LogMessage.errorfilecreation("Error inside the TMDataPacket:- "
						+ e.getMessage());
			}
			Date now = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			// utcFormat.setTimeZone(TimeZone.getTimeZone("IST"));

			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			Date currentTime = sdf.parse(sdf.format(now));

            if(gValid == 1  && pStatus == 0){
//                System.out.println("inside if");
                publishToPubNub(unitNo, lait, logit,ignition,mainSupply);
            }

//			if (!(lait == 0.00000)
//					&& !(logit == 0.00000)
//					&& ((Math.abs(currentTime.getTime() - date.getTime())) / 1000) < 1000) {
//				publishToPubNub(unitNo, lait, logit,ignition,mainSupply);
//			}
		}

	}

	public void publishToPubNub(String unitNo, double lait, double logit, int ignition, String mainSupply)
			throws IOException {
		// TODO Auto-generated method stub
		String channel = null;
		try {
			HashMap<Object, Object> obj = new HashMap<Object, Object>();
			obj.put("device_id", unitNo);
			obj.put("lat", lait);
			obj.put("lng", logit);
			obj.put("ignition", ignition);
			obj.put("mainSupply", mainSupply);

//			System.out.println("inside pub method");
//			if (unitNo.equals("355217044865989")) {
			if (unitNo.equals("355217046676483")) {

				obj.put("vehicle_id", 1);
				channel = "channel-1";
//			} else if (unitNo.equals("353218071614755")) {
			} else if (unitNo.equals("353218074676959")) {

				obj.put("vehicle_id", 2);
				channel = "channel-2";
			}
//			else if (unitNo.equals("355217044868611")) {
//
//				obj.put("vehicle_id", 3);
//				channel = "channel-3";
//			}

			pubNub.publish().message(obj).channel(channel)
					.async(new PNCallback<PNPublishResult>() {
						@Override
						public void onResponse(PNPublishResult result,
								PNStatus status) {
							// handle publish result, status always
							// present, result if successful
							// status.isError to see if error
							// happened
							if (status.isError()) {

								System.out.println(status.getErrorData().toString());
							}
						}
					});

		} catch (Exception ex) {
			LogMessage
					.errorfilecreation("Error inside the publishToPubNub method:- "
							+ ex.getMessage());
		}
	}

}
